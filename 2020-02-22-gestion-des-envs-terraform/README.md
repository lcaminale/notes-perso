# Bonnes pratiques terraform ?

                                  
![terraform modules](https://media.giphy.com/media/oYtVHSxngR3lC/giphy.gif "Modules terraform réactions 😎")


Les modules :
=============

Un module terraform est un ensemble de ressources qui sont utilisées ensemble.
D'un point de vue langage de programmation on peut l'apparanter à une fonction.

*Exemple:*

```python
def get_my_favourite_colour() -> str:
    return "blue"

# Other places in your code
get_my_favourite_colour()
```

**Ca permet de :**

* Réutiliser le code
* Faire de l'encapsulation de modules 
* Faire des abstractions (en terme d'architecture plutot qu'en ressource technique) --> voir l'exemple ci-dessous

```
.
├── modules
    ├── relational-database
    │   ├── main.tf
    │   ├── variables.tf
    │   ├── outputs.tf
    │   └── README.md
    └── no-relational-databse
        ├── main.tf
        ├── variables.tf 
        ├── outputs.tf
        └── README.md
```


Quand crée un module ?
----------------------

![](https://www.terraform.io/docs/cloud/guides/recommended-practices/images/image2-12cb4459.png "diagram module terraform")

Tips:
-----

* Un readme par module pour expliquer comment on l'utilise --> c'est chiant ... ok un utilitaire existe pour vous aidez : [terraform-docs](https://github.com/terraform-docs/terraform-docs)
* Créer un module s'il 

Useful links:
-------------

* terraform docs : https://www.terraform.io/docs/language/modules/develop/index.html


Registry modules :
==================

Un repo git centralisant des modules terraform. 

**Ca permet de :**

* Rendre accessible des modules terraform à d'autres projets
* Versionner les modules
* Avoir des revues de code x-projets (plus de retours) 

Useful links:
-------------

* article :  https://wahlnetwork.com/2020/08/11/using-private-git-repositories-as-terraform-modules/
* terraform aws : https://github.com/terraform-aws-modules 


Wrapper:
========

Facilite la vie du developpeur.

Il faut le voir comme un makefile, qui va cacher des commandes terraform derriere.


terragrunt:
-----------

Est l'un des wrapper les plus utilisés.

Ca permet de:

* Avoir des confs dry
* Forcer l'utilisation des modules terraform
* Gere le split des tfstate par dossier
* Cacher des commandes lourdes terraform ( du genre un `terraform init -backend-config=path/to/config/env.conf`) 
pour les mettre sous format code

useful links :
--------------

* https://github.com/gruntwork-io/terragrunt

⚠️  Attention : ne pas trop se coupler avec !


Séparer env dev et prod : 
=========================

* Petit tuto qui peut donner des idées : https://learn.hashicorp.com/tutorials/terraform/organize-configuration?in=terraform/modules

